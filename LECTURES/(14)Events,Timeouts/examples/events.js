var child = document.getElementsByClassName('child')[0],
	parent = document.getElementsByClassName('parent')[0];


child.addEventListener('click', function(event){
	console.log('CHILD', event, this);

	var e = new CustomEvent('custom', {
		detail: {
			date: new Date()
		}
	});
	
	document.dispatchEvent(e);
}, false);


document.addEventListener('custom', function(event){
	console.log('DOCUMENT', event, this);

	// event.stopPropagation();
}, false);

