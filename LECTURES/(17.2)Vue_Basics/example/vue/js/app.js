const app = new Vue({
	el: '.todoapp',
	data: {
		currentTask: "Task 1",
		todos: ["Task 1", "Task 2"]
	},
	methods:{
		addTodo(event){
			this.todos.push(this.currentTask);
		},
		removeTodo(todo){
			this.todos.splice(this.todos.indexOf(todo), 1)
		}
	}
})