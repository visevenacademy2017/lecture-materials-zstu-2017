module.exports = function (scope) {
	scope.patientsCount = 12;
	scope.maxPatientsCount = 20;

	scope.changeCarouselRotation = function () {
		scope.m.myCarousel.rotation = scope.m.mySlider.value;
	}
};