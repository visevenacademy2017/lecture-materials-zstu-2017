var globFs = null;

function errorCb(e){ console.error(e) }

function initFs(fs){
	globFs = fs;
}

window.webkitRequestFileSystem(window.TEMPORARY, 1024*1024*5, initFs, errorCb);

function writeToFile(object, fileName){
	globFs.root.getFile(fileName+'.json', {create: true}, function(fileEntry) {

		fileEntry.createWriter(function(fileWriter) {

			fileWriter.seek(0);

			var b = new Blob([JSON.stringify(object)], {type: 'text/plain'});

			fileWriter.write(b);

		}, errorCb);

	}, errorCb);
}

function readFromFile(fileName){
	globFs.root.getFile(fileName+'.json', {}, function(fileEntry) {

		fileEntry.file(function(file) {
			var reader = new FileReader();

			reader.onloadend = function(e) {
				console.log(JSON.parse(this.result))
			};

			reader.readAsText(file);
		}, errorCb);
	})
}

function removeFile(fileName){
	globFs.root.getFile(fileName+'.json', {create: false}, function(fileEntry) {

		fileEntry.remove(function() {
			console.log('File '+fileName+' removed.');
		});

	}, function(){
		console.log("Can't remove file with name: "+fileName)
	});
}