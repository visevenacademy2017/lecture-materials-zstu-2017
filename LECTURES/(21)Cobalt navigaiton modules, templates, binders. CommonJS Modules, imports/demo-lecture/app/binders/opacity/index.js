var rivets = require('rivets')

rivets.binders.opacity = (element,value)=>{
    element.style.opacity = value;
}