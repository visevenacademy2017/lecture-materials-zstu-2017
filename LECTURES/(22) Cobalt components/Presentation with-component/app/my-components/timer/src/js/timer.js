import {
	Component
} from 'component';

export class Timer extends Component {
	constructor() {
		super(...arguments);

		setInterval(() => {
			if (this.value < this.end) {
				this.value += 1;
			} else {
				this.endReached = true;
			}
		}, this.updateInterval)
	}

	ready(view) {

	}
	resetValue(event, that) {
		that.value = 0;
	}
	get defaults() {
		return {
			value: {
				type: 'Number',
				value: 0
			},
			updateInterval: {
				type: 'Number',
				value: 50
			},
			end: {
				type: 'Number',
				value: 50
			},
			valueLabel: {
				type: "Text",
				value: "Current value",
				sealed: true
			}
		};
	}

	refresh(prop) {

	}

	activate(targetEl) {

	}

	deactivate(targetEl) {

	}

	template() {
		return require('../template.html');
	}
}