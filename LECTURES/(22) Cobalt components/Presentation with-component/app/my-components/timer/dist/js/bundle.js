(function(){function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s}return e})()({1:[function(require,module,exports){
'use strict';

var createComponent = require('component').createComponent,
    Timer = require('./src/js/timer').Timer;

module.exports = createComponent({
	name: 'timer',
	constructor: Timer,
	localizationTemplate: 'my-components/timer/i18n/{lang}.json',
	localExtensions: {
		components: {}
	}
});require("rivets").components[module.exports.name] = module.exports;

},{"./src/js/timer":9,"component":3,"rivets":"rivets"}],2:[function(require,module,exports){
module.exports={
	"localizationPath": "components/{name}/i18n/{lang}.json"
}
},{}],3:[function(require,module,exports){
'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var rivets = require('rivets');
var utils = require('utils');
var ComponentConstructor = require('./src/js/ComponentConstructor.js').ComponentConstructor;
var buildModel = require('./src/js/buildModel.js');
var buildObservations = require('./src/js/buildObservations.js');
var config = require('./config.json');

function registerComponent(name, Constructor, localExtensions) {
  rivets.components[name] = createComponent(name, Constructor, localExtensions);
}

function createComponent(options, Constructor, localExtensions) {
  var name;

  if ((typeof options === 'undefined' ? 'undefined' : _typeof(options)) === 'object') {
    name = options.name;
    Constructor = options.constructor;
    localExtensions = options.localExtensions;
  } else {
    name = options;
  }

  Constructor.prototype.localizationTemplate = options.localizationTemplate || config.localizationPath;

  var descriptor = {
    name: name,
    block: Constructor.block,
    static: getStaticProperties(Constructor.prototype.defaults),
    template: Constructor.prototype.template,
    constructor: Constructor,
    initialize: function initialize(element, data) {
      var defaultProperties = utils.toCamelCaseObject(Constructor.prototype.defaults);
      var model = buildModel(defaultProperties, data, name);
      var componentInstance = new Constructor(element, model, this.view, this);

      Constructor.prototype.name = name;

      buildObservations.call(this, componentInstance, defaultProperties, data.model || {});

      return componentInstance;
    },
    unbind: function unbind() {
      // TODO: make unobserve..
    }
  };

  if (localExtensions) {
    rivets._.extensions.forEach(function (extensionName) {
      var extension = localExtensions[extensionName];

      if (extension) {
        descriptor[extensionName] = extension;
      }
    });
  }

  return descriptor;
}

function getStaticProperties(properties) {
  return Object.keys(properties).filter(function (property) {
    return isStaticProperty(properties, property);
  }).map(function (property) {
    return utils.toCamelCase(property);
  });
}

function isStaticProperty(properties, property) {
  return properties[property] && properties[property].static;
}

module.exports = registerComponent;
module.exports.createComponent = createComponent;
module.exports.Component = ComponentConstructor;

},{"./config.json":2,"./src/js/ComponentConstructor.js":4,"./src/js/buildModel.js":7,"./src/js/buildObservations.js":8,"rivets":"rivets","utils":"utils"}],4:[function(require,module,exports){
'use strict';

var _Promise = typeof Promise === 'undefined' ? require('es6-promise').Promise : Promise;

var utils = require('utils');
var builKeypath = require('./buildKeypath.js');
var getLocalizationByConstructor = require('./buildLocalization.js');
var generalModel = require('general-model');
var dispatcher = require('dispatcher');

function ComponentConstructor(element, model, view, binding) {
  this.element = element;
  this.parentScope = view.models;
  utils.mixin(this, model);
  this.element.model = this;
  this.element.view = {
    subscribe: subscribeToView.bind(this)
  };
  this.element.setAttribute('co-component', '');
  this.model = model;
  this.view = view;
  this.binding = binding;
  this.element.addEventListener('activate', handleActivation.bind(this));
  this.element.addEventListener('deactivate', handleDeactivation.bind(this));

  this.setIsConfigurable();
}

ComponentConstructor.prototype.isCommon = function () {
  return getParentKeypathes(this).some(function (keypath) {
    return utils.startsWith(keypath, 'm.common');
  });
};

ComponentConstructor.prototype.getTextElementsByProperty = function (property) {
  return utils.toArray(this.element.querySelectorAll('[rv-html]')).filter(function (element) {
    return element.getAttribute('rv-html') === property;
  }).filter(function (element) {
    return element.scope === this;
  }, this);
};

ComponentConstructor.prototype.getLocalization = function (lang) {
  return getLocalizationByConstructor(this.__proto__.constructor, lang);
};

ComponentConstructor.prototype.getAbsoluteKeypath = function () {
  return builKeypath(this.parentScope, this.getKeypath());
};

ComponentConstructor.prototype.getKeypath = function () {
  return this.element.getAttribute('model') || '';
};

ComponentConstructor.prototype.toJSON = function () {
  var that = this;
  return Object.keys(this).reduce(function (acc, property) {
    if (that.model.hasOwnProperty(property)) {
      acc[property] = that[property];
    }
    return acc;
  }, {});
};

ComponentConstructor.prototype.setIsConfigurable = function () {
  var keypath = this.getKeypath();
  Object.defineProperty(this, 'isConfigurable', {
    value: utils.startsWith(keypath, 'm.'),
    enumerable: true,
    configurable: false,
    writable: true
  });
};

ComponentConstructor.prototype.getLabel = function (lang) {
  lang = lang || 'en';

  return new _Promise(function (resolve, reject) {
    var label = getLabelFromElement(this.element);

    if (label) {
      resolve(label);
    } else {
      getLocalizationByConstructor(this.__proto__.constructor, lang).then(function (localization) {
        resolve(localization.name);
      }).catch(function (err) {
        resolve(getNameFromTagNameComponent(this.name || this.element.tagName.toLowerCase()));
      }.bind(this));
    }
  }.bind(this));
};

ComponentConstructor.prototype.subscribe = function (callback) {
  return new generalModel.ModelNotifier().observe(this.model).subscribe(function (model) {
    callback(model);
  }, true);
};

function getNameFromTagNameComponent(coTagName) {
  var normalName = coTagName.replace('co-', '').split('-').join(' ');

  return normalName[0].toUpperCase() + normalName.substring(1, normalName.lenght);
}

function handleActivation(event) {
  if (typeof this.activate === 'function' && isCobaltEvent(event)) {
    var prevModelState = utils.parse(this);
    this.activate(event.target);
    this.difference = utils.difference(prevModelState, utils.parse(this));
  }
}

function handleDeactivation(event) {
  if (typeof this.deactivate === 'function' && isCobaltEvent(event)) {
    this.deactivate(event.target);
  }

  utils.deepMixin(this, this.difference);
}

function isCobaltEvent(event) {
  return event.detail && !!event.detail.isCobaltEvent;
}

function getParentKeypathes(scope, keypathes) {
  keypathes = keypathes || [];
  if (scope && typeof scope.getKeypath === 'function') {
    keypathes.push(scope.getKeypath());
    getParentKeypathes(scope.parentScope, keypathes);
  }

  return keypathes;
}

function getLabelFromElement(element) {
  return element.getAttribute('user-label');
}

function subscribeToView(callback) {
  var _this = this;

  dispatcher.subscribeEvent(function (event) {
    if (event.type === dispatcher.EventType.DOM_REFRESHED && event.element === _this.element) {
      callback(_this.element);
    }
  });
}

exports.ComponentConstructor = ComponentConstructor;

},{"./buildKeypath.js":5,"./buildLocalization.js":6,"dispatcher":"dispatcher","es6-promise":"es6-promise","general-model":"general-model","utils":"utils"}],5:[function(require,module,exports){
'use strict';

function buildAbsoluteKeypath(parentPath, keypath) {
	var key = removeAlias(keypath);

	if (parentPath) {
		return key ? parentPath + '.' + removeAlias(keypath) : parentPath;
	} else {
		return keypath;
	}
}

function collectParentsKeypath(model, keypathes) {
	keypathes = keypathes || [];
	if (model && model.keypath) {
		keypathes.unshift(keypathModifier(model), model.index);
		collectParentsKeypath(model.__proto__, keypathes);
	}

	return keypathes;
}

function keypathModifier(model) {
	if (model.__proto__.keypath) {
		return removeAlias(model.keypath);
	}

	return model.keypath;
}

function removeAlias(keypath) {
	return keypath.split('.').slice(1).join('.');
}

module.exports = function (parentScope, keypath) {
	return buildAbsoluteKeypath(collectParentsKeypath(parentScope).join('.'), keypath);
};

},{}],6:[function(require,module,exports){
'use strict';

var _loader = require('loader');

var _utils = require('utils');

var _Promise = typeof Promise === 'undefined' ? require('es6-promise').Promise : Promise;

module.exports = getLocalizationByConstructor;

function getLocalizationByConstructor(constructor, lang) {
	//TODO: works only if localizationTemplate and name were added to the prototype during component registration
	var path = (0, _utils.template)(constructor.prototype.localizationTemplate, { name: constructor.prototype.name, lang: lang });

	return (0, _loader.loadJSON)(path).then(function (localization) {
		return extendLocalizationByScheme(localization.defaults, constructor.prototype.defaults, lang).then(function () {
			return localization;
		});
	});
}

//if property described in scheme has 'base' defined, it's localization is extended with localization from 'base' 
function extendLocalizationByScheme(localization, scheme, lang) {
	var localizationPromises = [];

	var _loop = function _loop(prop) {
		var propertyDescriptor = scheme[prop],
		    localizationLoadingPromise = void 0;

		if (propertyDescriptor.sealed) {
			return 'continue';
		}

		if (propertyDescriptor.base) {
			localizationLoadingPromise = getLocalizationByConstructor(propertyDescriptor.base, lang).then(function (subLocalization) {
				return localization[prop].defaults = subLocalization.defaults;
			});
		} else if (propertyDescriptor.scheme) {
			localizationLoadingPromise = extendLocalizationByScheme(localization[prop].defaults, propertyDescriptor.scheme, lang);
		} else {
			return 'continue';
		}
		localizationPromises.push(localizationLoadingPromise);
	};

	for (var prop in scheme) {
		var _ret = _loop(prop);

		if (_ret === 'continue') continue;
	}

	return _Promise.all(localizationPromises);
}

},{"es6-promise":"es6-promise","loader":"loader","utils":"utils"}],7:[function(require,module,exports){
"use strict";

var utils = require("utils");

function buildModel(defaultProperties, data, name) {
	var defaultModel = getDefaultModel(defaultProperties),
	    elementModel = utils.toCamelCaseObject(data.model || {}),
	    attributesData = getDataFromAttributes(defaultProperties, data),
	    model = mixin(mixin(defaultModel, elementModel, defaultProperties), attributesData, defaultProperties);

	return model;
}

function mixin(target, source, defaultProperties) {
	Object.keys(source).forEach(function (property) {
		if (source.hasOwnProperty(property) && defined(source[property]) && defaultProperties.hasOwnProperty(property)) {
			if (target[property] && target[property].constructor === Object) {
				addMissingProperies(defaultProperties[property].value, source[property]); // add missing properties and keep reference for correct observing
			}
			target[property] = source[property];
		}
	});

	return target;
}

function addMissingProperies(defaultProperties, source) {
	return Object.keys(defaultProperties).forEach(function (property) {
		if (!source.hasOwnProperty(property)) {
			source[property] = defaultProperties[property];
		}
	});
}

function getDefaultModel(defaultProperties) {
	return Object.keys(defaultProperties).reduce(function (acc, property) {
		acc[property] = defaultProperties[property].value;
		return acc;
	}, {});
}

function getDataFromAttributes(defaultProperties, data) {
	return Object.keys(defaultProperties).reduce(function (acc, property) {
		if (data.hasOwnProperty(property)) {
			if (defaultProperties[property] && defaultProperties[property].static) {
				acc[property] = utils.revive(data[property]);
			} else {
				acc[property] = data[property];
			}
		}
		return acc;
	}, {});
}

function defined(value) {
	return value !== undefined && value !== null;
}

module.exports = buildModel;

},{"utils":"utils"}],8:[function(require,module,exports){
'use strict';

var generalModel = require('general-model');
var complexProperties = [Object, Array, DataView, 'Object', 'Array', 'DataView'];

module.exports = function buildObservations(componentInstance, defaultProperties, model) {
  Object.keys(defaultProperties).forEach(function (property) {
    model[property] = componentInstance[property];

    if (isRefreshableProperty(defaultProperties, property) && isComplexProperty(defaultProperties, property)) {
      observeComplexProperty(property, componentInstance);
    }

    this.observe(componentInstance, property, function () {
      if (isRefreshableProperty(defaultProperties, property)) {
        componentInstance.refresh(property);
      }
      componentInstance.model[property] = componentInstance[property];
      model[property] = componentInstance[property];
    });

    this.observe(componentInstance.model, property, function () {
      componentInstance[property] = componentInstance.model[property];
    });

    this.observe(model, property, function () {
      componentInstance[property] = model[property];
    });
  }, this);
};

function observeComplexProperty(property, componentInstance) {
  // setTimeout because nested bindings...
  setTimeout(function () {
    new generalModel.ModelNotifier().deepObserve(componentInstance, property).subscribe(function (model) {
      componentInstance.refresh(property);
    }, true);
  }, 50);
}

function isComplexProperty(defaultProperties, property) {
  var isComplexProp = complexProperties.indexOf(defaultProperties[property].type) !== -1;
  return !isComplexProp ? Array.isArray(defaultProperties[property].type) : isComplexProp;
}

function isRefreshableProperty(defaultProperties, property) {
  return !!defaultProperties[property].refresh;
}

},{"general-model":"general-model"}],9:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.Timer = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _component = require('component');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Timer = exports.Timer = function (_Component) {
	_inherits(Timer, _Component);

	function Timer() {
		_classCallCheck(this, Timer);

		var _this = _possibleConstructorReturn(this, (Timer.__proto__ || Object.getPrototypeOf(Timer)).apply(this, arguments));

		setInterval(function () {
			if (_this.value < _this.end) {
				_this.value += 1;
			} else {
				_this.endReached = true;
			}
		}, _this.updateInterval);
		return _this;
	}

	_createClass(Timer, [{
		key: 'ready',
		value: function ready(view) {}
	}, {
		key: 'resetValue',
		value: function resetValue(event, that) {
			that.value = 0;
		}
	}, {
		key: 'refresh',
		value: function refresh(prop) {}
	}, {
		key: 'activate',
		value: function activate(targetEl) {}
	}, {
		key: 'deactivate',
		value: function deactivate(targetEl) {}
	}, {
		key: 'template',
		value: function template() {
			return require('../template.html');
		}
	}, {
		key: 'defaults',
		get: function get() {
			return {
				value: {
					type: 'Number',
					value: 0
				},
				updateInterval: {
					type: 'Number',
					value: 50
				},
				end: {
					type: 'Number',
					value: 50
				},
				valueLabel: {
					type: "Text",
					value: "Current value",
					sealed: true
				}
			};
		}
	}]);

	return Timer;
}(_component.Component);

},{"../template.html":10,"component":3}],10:[function(require,module,exports){
module.exports = '<co-text class="value" html="valueLabel" timer="">Current value</co-text><co-container class="value" rv-text="value" fixed="removing" user-label="Value" timer=""></co-container><div class="animated" rv-class-active="endReached" timer=""><content timer=""></content><button rv-on-click="resetValue" timer="">Reset value</button></div>'
},{}]},{},[1]);
