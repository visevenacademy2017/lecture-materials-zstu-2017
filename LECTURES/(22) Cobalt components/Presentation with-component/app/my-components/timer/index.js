var createComponent = require('component').createComponent,
	Timer = require('./src/js/timer').Timer;

module.exports = createComponent({
	name: 'timer',
	constructor: Timer,
	localizationTemplate: 'my-components/timer/i18n/{lang}.json',
	localExtensions: {
		components: {

		}
	}
});