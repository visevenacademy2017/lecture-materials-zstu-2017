const rootElement = document.getElementById('app')
const model = {
    tasks: [
        {
            name: "Commit changes",
            isDone: false
        },
        {
            name: "Read",
            isDone: true
        },
        {
            name: "Walk",
            isDone: false
        },
        {
            name: "Learn",
            isDone: false
        },
    ],
    newTaskName: '',
    addTask(event, model) {
        model.todo.tasks.push({
            name: model.todo.newTaskName,
            isDone: false
        })

        model.todo.newTaskName = ''
    },
    removeTask(event, model){
        model.todo.tasks.splice(model.todo.index, 1)
    },
    removeAllDoneTasks(event, model){
        model.todo.tasks
            .filter(task => task.isDone)
            .forEach(task => {
                const indexToRemove = model.todo.tasks.findIndex(item => {
                    return task === item
                })

                model.todo.tasks.splice(indexToRemove, 1)
            })
    }
}

rivets.binders.done = function (element, value) {
    if (value) {
        element.style.textDecoration = 'line-through'
        element.style.opacity = '0.5'
    } else {
        element.style.textDecoration = 'none'
        element.style.opacity = '1'
    }
}

rivets.components['super-image'] = {
    template(){
        return `
            <div>
              <img rv-src="path" style="height: 300px; width: 400px; transition: 2s" rv-on-click="flip" />
            </div>
        `
    },
    initialize(element, data) {
        let rotate = 0
        return Object.assign({
            path: './images/img1.jpeg',
            flip(event){
                event.target.style.transform = `rotateY(${ rotate += 180}deg)`
            }
        }, data)
    }
}

rivets.formatters.toUpper = function(value){
    return value.toUpperCase()
}

rivets.bind(rootElement, {
    todo: model,
    test: {
        path: './images/img2.jpeg'
    }
})